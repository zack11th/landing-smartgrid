export default function () {
    let tab = $('.portfolio__tab');
    let links = $('.portfolio__link');

    function shows(target) {
        let duration = 100;
        links.fadeOut(duration);
        setTimeout(function () {
            if (target === 'all') {
                links.fadeIn(duration);
            } else {
                links.each(function (i) {
                    if ($(this).data('target') === target) {
                        $(this).fadeIn(duration);
                    }
                });
            }
        }, duration);
    }

    tab.on('click', function (e) {
        if ($(this).hasClass('portfolio__tab--active')) {
            return;
        }
        tab.removeClass('portfolio__tab--active');
        $(this).addClass('portfolio__tab--active');
        let target = $(this).data('target');
        shows(target);
    });
}