export default function () {
    let title = $('.accordion__title');
    let descr = $('.accordion__descr');

    title.on('click', function () {
        $(this).toggleClass('accordion__title--active');
        title.not(this).removeClass('accordion__title--active');
        let current = $(this).next().stop(true).slideToggle(500);
        title.next().not(current).slideUp(500);
    });
}