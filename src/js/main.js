import accordion from './accordion';
import portfolio from './portfolio';
import carousel from './carousel';

window.addEventListener('load', function () {
    accordion();
    portfolio();
    carousel();
});