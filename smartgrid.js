module.exports = {
    columns: 12,
    offset: "30px",
    outputStyle: "sass",
    //mobileFirst: true,
    container: {
        maxWidth: "1200px",
        fields: "30px" // поля по краям сайта (не должны быть меньше половины offset
    },
    breakPoints: {
        md: {
            width: "992px",
            fields: "15px"
        },
        xmd: {
            width: "860px"
        },
        sm: {
            width: "720px"
        },
        xs: {
            width: "576px"
        },
        xxs: {
            width: "420px",
            /*
            offset: "10px",
            fields: "5px"
            */
        }
    },
    //detailedCalc: true
};